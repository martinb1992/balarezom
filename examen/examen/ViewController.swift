//
//  ViewController.swift
//  examen
//
//  Created by Martin Balarezo on 5/6/18.
//  Copyright © 2018 Martin Balarezo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    
    @IBOutlet weak var inputTextField: UITextField!
    
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var auxLabel: UILabel!
    
    @IBAction func nextButton(_ sender: Any) {
        
        auxLabel.text = "clic"
        
        let urlString = "https://api.myjson.com/bins/72936"
        
        let url = URL(string: urlString)
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: url!) { (data, response, error) in
            print(data)
            guard let data = data else{
                print("Error NO data")
                return
            }
            
            guard let jsonInfo = try? JSONDecoder().decode(jsonInfo.self, from: data) else {
                print("Error Decoding")
                return
                
            }
            
            print(jsonInfo.info[0].date)
            
            DispatchQueue.main.async {
                
                self.resultLabel.text = "\(jsonInfo.info[0].viewTitle)"
            }
            
            
            
        }
        
        task.resume()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

