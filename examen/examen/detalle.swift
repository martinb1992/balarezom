//
//  detalle.swift
//  examen
//
//  Created by Martin Balarezo on 5/6/18.
//  Copyright © 2018 Martin Balarezo. All rights reserved.
//

import Foundation

struct jsonDetalle: Decodable {
    let detalle: [infoDetalle]
    
}
struct infoDetalle: Decodable {
    let label: String
    let value: Int
    
}
