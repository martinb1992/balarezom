//
//  jsonController.swift
//  examen
//
//  Created by Martin Balarezo on 5/6/18.
//  Copyright © 2018 Martin Balarezo. All rights reserved.
//

import Foundation

struct jsonInfo: Decodable {
    let info: [infoJson]
    
    
}

struct infoJson: Decodable {
    let viewTitle: String
    let date: String
    
}

struct allJson: Decodable{
    let title: String
    let date: String
    let nextLink: String
}
